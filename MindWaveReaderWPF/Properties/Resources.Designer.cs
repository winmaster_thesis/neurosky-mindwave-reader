﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MindWaveReaderWPF.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MindWaveReaderWPF.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data: .
        /// </summary>
        internal static string Data {
            get {
                return ResourceManager.GetString("Data", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to title.
        /// </summary>
        internal static string FileTitle {
            get {
                return ResourceManager.GetString("FileTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mental effort: .
        /// </summary>
        internal static string MentalEffort {
            get {
                return ResourceManager.GetString("MentalEffort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving to .csv file failed.
        /// </summary>
        internal static string SavingFailed {
            get {
                return ResourceManager.GetString("SavingFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving file not possible due to small amount or lack of collected data..
        /// </summary>
        internal static string SavingFileNotPossible {
            get {
                return ResourceManager.GetString("SavingFileNotPossible", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving to .csv file completed successfully.
        /// </summary>
        internal static string SavingSuccessfull {
            get {
                return ResourceManager.GetString("SavingSuccessfull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Task familiarity: .
        /// </summary>
        internal static string TaskFamiliarity {
            get {
                return ResourceManager.GetString("TaskFamiliarity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Try connect MindWave Mobile and record some data.
        /// </summary>
        internal static string TryConnectMindWave {
            get {
                return ResourceManager.GetString("TryConnectMindWave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ----Values computed after a few seconds: -----.
        /// </summary>
        internal static string ValuesSpacer {
            get {
                return ResourceManager.GetString("ValuesSpacer", resourceCulture);
            }
        }
    }
}
